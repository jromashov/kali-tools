---
Title: humble
Homepage: https://github.com/rfc-st/humble
Repository: https://gitlab.com/kalilinux/packages/humble
Architectures: all
Version: 1.33-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### humble
 
  This package contains an  humble, and fast, security-oriented HTTP headers
  analyzer.
 
 **Installed size:** `226 KB`  
 **How to install:** `sudo apt install humble`  
 
 {{< spoiler "Dependencies:" >}}
 * publicsuffix
 * python3
 * python3-colorama
 * python3-fpdf
 * python3-requests
 * python3-tldextract
 {{< /spoiler >}}
 
 ##### humble
 
 
 ```
 root@kali:~# humble -h
 usage: humble.py [-h] [-a] [-b] [-df] [-e [PATH]] [-f [TERM]] [-g] [-l {es}]
                  [-o {csv,html,json,pdf,txt}] [-op OUTPUT_PATH] [-r] [-u URL]
                  [-ua USER_AGENT] [-v]
 
 'humble' (HTTP Headers Analyzer) | https://github.com/rfc-st/humble |
 v.2024-01-19
 
 options:
   -h, --help                  show this help message and exit
   -a                          show statistics of the performed analysis (will
                               be global if '-u' is omitted)
   -b                          show a brief analysis (if omitted, a detailed
                               one will be shown)
   -df                         do not follow redirects (if omitted, the last
                               redirection will be the one analyzed)
   -e [PATH]                   show TLS/SSL checks (requires the PATH of
                               https://testssl.sh/ and Unix machine)
   -f [TERM]                   show fingerprint statistics (will be the Top 20
                               if "TERM", e.g. "Google", is omitted)
   -g                          show guidelines for securing popular web
                               servers/services
   -l {es}                     the language for displaying analyses, errors and
                               messages (if omitted it will be in English)
   -o {csv,html,json,pdf,txt}  save analysis to 'scheme_host_port_yyyymmdd.ext'
                               file (csv/json files will contain a brief
                               analysis)
   -op OUTPUT_PATH             save analysis to OUTPUT_PATH (if omitted, the
                               PATH of 'humble.py' will be used)
   -r                          show HTTP response headers and a detailed
                               analysis ('-b' parameter will take priority)
   -u URL                      scheme, host and port to analyze. E.g.
                               https://google.com
   -ua USER_AGENT              User-Agent ID from 'additional/user_agents.txt'
                               to use. '0' will show all and '1' is the
                               default.
   -v, --version               check for updates at https://github.com/rfc-
                               st/humble
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
